package com.infotech.maz.tffinwala;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Change_Password extends AppCompatActivity {

 EditText edt_pass,edt_confo_pass;
    Button btn_update;
    String pass="";
    boolean flag=false;
    String email;
    public static final String URL_UPDATE_PASS="http://tiffinwala.16mb.com/update_password.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change__password);

        email=getIntent().getStringExtra("Email");

        edt_pass=(EditText)findViewById(R.id.activity_chane_pass_edt_pass);
        edt_confo_pass=(EditText)findViewById(R.id.activity_chane_pass_edt_confo_pass);
        btn_update=(Button)findViewById(R.id.activity_change_pass_btn_upade);

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pass=edt_pass.getText().toString();
                String conf_pass=edt_confo_pass.getText().toString();

                if(pass.equals(conf_pass)){

                    new ForgotPasswordCheck().execute();

                }
                else {
                    Toast.makeText(Change_Password.this, "Password NOT Match..!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    class ForgotPasswordCheck extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        public String doInBackground(String... args) {
            // creating new product in background thread


            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();


                params.add(new BasicNameValuePair("Email", email));

                params.add(new BasicNameValuePair("Password", pass));


                JSONParser jParser =new JSONParser();

                JSONObject json = jParser.makeHttpRequest(URL_UPDATE_PASS, "POST", params);


                int success1 = Integer.parseInt(json.getString("success"));
                Log.e("data", params.toString());
                Log.d("success", json.toString());

                if (success1 == 1) {

                    //    Toast.makeText(Complete_Profile.this, "Registration Successfully..!!", Toast.LENGTH_SHORT).show();
                    //    startActivity(new Intent(Complete_Profile.this,LoginActivity.class));
                    flag=true;

                }


                else {
                    String success2 = json.getString("success");
                    Log.d("success", success2);
                    flag=false;
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String result) {
            if(flag){

                Toast.makeText(Change_Password.this, "Password Changed...!!", Toast.LENGTH_SHORT).show();
                Intent i =new Intent(Change_Password.this,LoginActivity.class);
                startActivity(i);

            }
            else {
                Toast.makeText(Change_Password.this, "OTP not sent Try Again.!!!", Toast.LENGTH_SHORT).show();
            }

            super.onPostExecute(result);

        }
    }
}
