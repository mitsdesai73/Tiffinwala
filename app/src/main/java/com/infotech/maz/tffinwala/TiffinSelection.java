package com.infotech.maz.tffinwala;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;





public class TiffinSelection extends AppCompatActivity {

FloatingActionButton fb_next;
    CheckBox chk_mini,chk_avg,chk_full;
    LinearLayout lin_mini,lin_avg,lin_full;
    TextView txt_mini,txt_avg,txt_full;
    TextView txt_mini_price,txt_avg_price,txt_full_price;

    Boolean IsMini=false;
    Boolean IsAvg=false;
    Boolean IsFull=false;



    private static final String TiffinDetails="http://tiffinwala.16mb.com/fetch_tiifin_details.php";

    TextView txt_sabji,txt_dal,txt_rice,txt_roti,txt_paratha,txt_pickel,txt_salad,txt_dessert,txt_tiifin_type,txt_papad;
    LinearLayout lin_sabji,lin_dal,lin_rice,lin_roti_paratha,lin_pickel,lin_salad,lin_dessert,lin_papad,lin_OR;
    JSONParser  jParser=new JSONParser();

    SharedPreferences pref_tiffin;
    SharedPreferences.Editor editor_tiffin;
    SharedPreferences pref_login;
    SharedPreferences.Editor editor_login;

ArrayList<String > Tiffin_Type_list=new ArrayList<String>();
    ArrayList<String> Tiffin_ID =new ArrayList<String>();
   ArrayList<String> Sabji =new ArrayList<String>();
    ArrayList<String> Dal =new ArrayList<String>();
    ArrayList<String> Rice =new ArrayList<String>();
    ArrayList<String> Roti =new ArrayList<String>();
    ArrayList<String> Paratha =new ArrayList<String>();
    ArrayList<String> Papad =new ArrayList<String>();
    ArrayList<String> Pickel =new ArrayList<String>();
    ArrayList<String> Salad =new ArrayList<String>();
    ArrayList<String> Price =new ArrayList<String>();
    ArrayList<String>  Dessert=new ArrayList<String>();
    HashMap<String,Integer> Tiffin_Type=new HashMap<String, Integer>();

    HashMap<String ,String > selected_tiffin=new HashMap<String, String>();


    boolean Add_Order=false;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tiffin_selection);
        final Dialog dialog = new Dialog(this);


        this.setTitle("Tiffin Type");


        pref_tiffin=getSharedPreferences("Tiffin_Type",MODE_PRIVATE);
        pref_login=getSharedPreferences("Login",MODE_PRIVATE);
        editor_tiffin=pref_tiffin.edit();
        editor_login=pref_login.edit();

        Add_Order=pref_login.getBoolean("Add_Order",false);

        fb_next=(FloatingActionButton)findViewById(R.id.activity_tiffin_selection_fb_next);

        chk_mini=(CheckBox)findViewById(R.id.activity_tiffin_selection_chk_mini);
        chk_avg=(CheckBox)findViewById(R.id.activity_tiffin_selection_chk_avg);
        chk_full=(CheckBox)findViewById(R.id.activity_tiffin_selection_chk_full);

        lin_mini=(LinearLayout)findViewById(R.id.activity_tiffin_selection_lin_mini);
        lin_avg=(LinearLayout)findViewById(R.id.activity_tiffin_selection_lin_avg);
        lin_full=(LinearLayout)findViewById(R.id.activity_tiffin_selection_lin_full);

        txt_mini=(TextView)findViewById(R.id.activity_tiffin_selection_txt_mini);
        txt_avg=(TextView)findViewById(R.id.activity_tiffin_selection_txt_avg);
        txt_full=(TextView)findViewById(R.id.activity_tiffin_selection_txt_full);

        dialog.setContentView(R.layout.custome_dialog_tiffin_details);
        txt_tiifin_type=(TextView)dialog.findViewById(R.id.custome_dialog_tiifin_txt_type);
        txt_sabji=(TextView)dialog.findViewById(R.id.custome_dialog_tiifin_txt_sabji);
        txt_dal=(TextView)dialog.findViewById(R.id.custome_dialog_tiifin_txt_dal);
        txt_rice=(TextView)dialog.findViewById(R.id.custome_dialog_tiifin_txt_rice);
        txt_roti=(TextView)dialog.findViewById(R.id.custome_dialog_tiifin_txt_roti);
        txt_paratha=(TextView)dialog.findViewById(R.id.custome_dialog_tiifin_txt_paratha);
        txt_pickel=(TextView)dialog.findViewById(R.id.custome_dialog_tiifin_txt_pickel);
        txt_salad=(TextView)dialog.findViewById(R.id.custome_dialog_tiifin_txt_salad);
        txt_dessert=(TextView)dialog.findViewById(R.id.custome_dialog_tiifin_txt_dessert);
        txt_papad=(TextView)dialog.findViewById(R.id.custome_dialog_tiifin_txt_papad);

        lin_sabji=(LinearLayout)dialog.findViewById(R.id.custome_dialog_tiifin_lin_sabji);
        lin_dal=(LinearLayout)dialog.findViewById(R.id.custome_dialog_tiifin_lin_dal);
        lin_rice=(LinearLayout)dialog.findViewById(R.id.custome_dialog_tiifin_lin_rice);
        lin_roti_paratha=(LinearLayout)dialog.findViewById(R.id.custome_dialog_tiifin_lin_roti_paratha);
        lin_pickel=(LinearLayout)dialog.findViewById(R.id.custome_dialog_tiifin_lin_pickel);
        lin_salad=(LinearLayout)dialog.findViewById(R.id.custome_dialog_tiifin_lin_salad);
        lin_dessert=(LinearLayout)dialog.findViewById(R.id.custome_dialog_tiifin_lin_dessert);
        lin_papad=(LinearLayout)dialog.findViewById(R.id.custome_dialog_tiifin_lin_papad);

        lin_OR=(LinearLayout)dialog.findViewById(R.id.custome_dialog_tiifin_txt_OR);


        txt_mini_price=(TextView)findViewById(R.id.activity_tiffin_selection_txt_mini_price);
        txt_avg_price=(TextView)findViewById(R.id.activity_tiffin_selection_txt_avg_price);
        txt_full_price=(TextView)findViewById(R.id.activity_tiffin_selection_txt_full_price);




        new CheckTiffin().execute();

        fb_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(TiffinSelection.this, "Data"+selected_tiffin, Toast.LENGTH_SHORT).show();

                Intent intent=new Intent(TiffinSelection.this,SelectMealDetails.class);
                startActivity(intent);

            }
        });


        txt_mini.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                lin_OR.setVisibility(View.VISIBLE);


                int i=0;
                txt_tiifin_type.setText(Tiffin_Type_list.get(i));
                if(Sabji.get(i).equals("0"))
                {
                    lin_sabji.setVisibility(View.GONE);
                }
                txt_sabji.setText(Sabji.get(i));


                if(Dal.get(i).equals("0"))
                {
                    lin_dal.setVisibility(View.GONE);
                }
                txt_dal.setText(Dal.get(i));


                if(Rice.get(i).equals("0"))
                {
                    lin_rice.setVisibility(View.GONE);
                }
                txt_rice.setText(Rice.get(i));


                if(Roti.get(i).equals("0"))
                {
                    lin_roti_paratha.setVisibility(View.GONE);
                }
                txt_roti.setText(Roti.get(i));

                if(Paratha.get(i).equals("0"))
                {
                    lin_roti_paratha.setVisibility(View.GONE);
                }
                txt_paratha.setText(Paratha.get(i));


                if(Papad.get(i).equals("0"))
                {
                    lin_papad.setVisibility(View.GONE);
                }
                else {
                    lin_papad.setVisibility(View.VISIBLE);

                }
                txt_papad.setText(Papad.get(i));

                if(Pickel.get(i).equals("0"))
                {
                    lin_pickel.setVisibility(View.GONE);
                }
                else {
                    lin_pickel.setVisibility(View.VISIBLE);

                }
                txt_pickel.setText(Pickel.get(i));


                if(Salad.get(i).equals("0"))
                {
                    lin_salad.setVisibility(View.GONE);
                }
                else {
                    lin_salad.setVisibility(View.VISIBLE);

                }
                txt_salad.setText(Salad.get(i));


                if(Dessert.get(i).equals("0"))
                {
                    lin_dessert.setVisibility(View.GONE);
                }
                else {
                    lin_dessert.setVisibility(View.VISIBLE);

                }
                txt_dessert.setText(Dessert.get(i));

                dialog.setTitle("Custom Alert Dialog");
                dialog.show();

            }
        });



        txt_avg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                lin_OR.setVisibility(View.GONE);

                int i=1;
                txt_tiifin_type.setText(Tiffin_Type_list.get(i));
                if(Sabji.get(i).equals("0"))
                {
                    lin_sabji.setVisibility(View.GONE);
                }

                txt_sabji.setText(Sabji.get(i));


                if(Dal.get(i).equals("0"))
                {
                    lin_dal.setVisibility(View.GONE);
                }
                txt_dal.setText(Dal.get(i));


                if(Rice.get(i).equals("0"))
                {
                    lin_rice.setVisibility(View.GONE);
                }
                txt_rice.setText(Rice.get(i));


                if(Roti.get(i).equals("0"))
                {
                    lin_roti_paratha.setVisibility(View.GONE);
                }
                txt_roti.setText(Roti.get(i));

                if(Paratha.get(i).equals("0"))
                {
                    lin_roti_paratha.setVisibility(View.GONE);
                }
                txt_paratha.setText(Paratha.get(i));

                if(Papad.get(i).equals("0"))
                {
                    lin_papad.setVisibility(View.GONE);
                }
                else {
                    lin_papad.setVisibility(View.VISIBLE);

                }
                txt_papad.setText(Papad.get(i));

                if(Pickel.get(i).equals("0"))
                {
                    lin_pickel.setVisibility(View.GONE);
                }
                else {
                    lin_pickel.setVisibility(View.VISIBLE);

                }
                txt_pickel.setText(Pickel.get(i));


                if(Salad.get(i).equals("0"))
                {
                    lin_salad.setVisibility(View.GONE);
                }
                else {
                    lin_salad.setVisibility(View.VISIBLE);

                }
                txt_salad.setText(Salad.get(i));


                if(Dessert.get(i).equals("0"))
                {
                    lin_dessert.setVisibility(View.GONE);
                }
                else {
                    lin_dessert.setVisibility(View.VISIBLE);

                }
                txt_dessert.setText(Dessert.get(i));

                dialog.setTitle("Custom Alert Dialog");
                dialog.show();

            }
        });


        txt_full.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                lin_OR.setVisibility(View.GONE);

                int i=2;
                txt_tiifin_type.setText(Tiffin_Type_list.get(i));
                if(Sabji.get(i).equals("0"))
                {
                    lin_sabji.setVisibility(View.GONE);
                }
                txt_sabji.setText(Sabji.get(i));


                if(Dal.get(i).equals("0"))
                {
                    lin_dal.setVisibility(View.GONE);
                }
                txt_dal.setText(Dal.get(i));


                if(Rice.get(i).equals("0"))
                {
                    lin_rice.setVisibility(View.GONE);
                }
                txt_rice.setText(Rice.get(i));


                if(Roti.get(i).equals("0"))
                {
                    lin_roti_paratha.setVisibility(View.GONE);
                }
                txt_roti.setText(Roti.get(i));

                if(Paratha.get(i).equals("0"))
                {
                    lin_roti_paratha.setVisibility(View.GONE);
                }
                txt_paratha.setText(Paratha.get(i));

                if(Papad.get(i).equals("0"))
                {
                    lin_papad.setVisibility(View.GONE);
                }
                else {
                    lin_papad.setVisibility(View.VISIBLE);

                }
                txt_papad.setText(Papad.get(i));

                if(Pickel.get(i).equals("0"))
                {
                    lin_pickel.setVisibility(View.GONE);
                }
                else {
                    lin_pickel.setVisibility(View.VISIBLE);

                }
                txt_pickel.setText(Pickel.get(i));


                if(Salad.get(i).equals("0"))
                {
                    lin_salad.setVisibility(View.GONE);
                }
                else {
                    lin_salad.setVisibility(View.VISIBLE);

                }
                txt_salad.setText(Salad.get(i));


                if(Dessert.get(i).equals("0"))
                {
                    lin_dessert.setVisibility(View.GONE);
                }
                else {
                    lin_dessert.setVisibility(View.VISIBLE);

                }
                txt_dessert.setText(Dessert.get(i));


                dialog.setTitle("Custom Alert Dialog");
                dialog.show();

            }
        });


        chk_mini.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                  editor_tiffin.putString("Mini Meal",Tiffin_ID.get(0));
                    editor_tiffin.commit();
                    lin_mini.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.ColorBackLayout));
                    selected_tiffin.put(Tiffin_Type_list.get(0),Tiffin_ID.get(0));


             }
                else {
                   editor_tiffin.remove("Mini Meal");
                    editor_tiffin.commit();
                    lin_mini.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.ColorBlack));
                    selected_tiffin.remove(Tiffin_Type_list.get(0));
                }
            }
        });


        chk_avg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    editor_tiffin.putString("Average Meal",Tiffin_ID.get(1));
                    editor_tiffin.commit();
                    lin_avg.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.ColorBackLayout));
                    selected_tiffin.put(Tiffin_Type_list.get(1),Tiffin_ID.get(1));

                }
                else {
                    editor_tiffin.remove("Avgerage Meal");
                    editor_tiffin.commit();
                    lin_avg.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.ColorBlack));
                    selected_tiffin.remove(Tiffin_Type_list.get(1));
                }

            }
        });


        chk_full.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    editor_tiffin.putString("Full Meal",Tiffin_ID.get(2));
                    editor_tiffin.commit();
                    lin_full.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.ColorBackLayout));
                    selected_tiffin.put(Tiffin_Type_list.get(2),Tiffin_ID.get(2));
                }
                else
                {
                    editor_tiffin.remove("Full Meal");
                    editor_tiffin.commit();
                    lin_full.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.ColorBlack));
                    selected_tiffin.remove(Tiffin_Type_list.get(2));
                }

            }
        });


        lin_mini.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(chk_mini.isChecked()){
                    startActivity(new Intent(TiffinSelection.this,SelectMealDetails.class));
                }
            }
        });

        lin_avg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(chk_avg.isChecked()){
                    startActivity(new Intent(TiffinSelection.this,SelectMealDetails.class));
                }
            }
        });

        lin_full.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(chk_full.isChecked()){
                    startActivity(new Intent(TiffinSelection.this,SelectMealDetails.class));
                }
            }
        });





    }

    @Override
    public void onBackPressed() {

        if(Add_Order){
            startActivity(new Intent(TiffinSelection.this,Welcome.class));
        }
        else {
            startActivity(new Intent(TiffinSelection.this,TiffinSelection.class));
        }


        super.onBackPressed();
    }


    class CheckTiffin extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        /**
         * getting All products from url
         */
        @Override
        public String doInBackground(String... args) {
            // creating new product in background thread


            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();


                params.add(new BasicNameValuePair("Tiffin_Type","Abc"));


                JSONObject json = jParser.makeHttpRequest(TiffinDetails, "POST", params);


                int success1 = Integer.parseInt(json.getString("success"));
                Log.e("data", params.toString());
                Log.d("success", json.toString());
                if (success1 == 1) {

                    JSONArray jsonArray=json.getJSONArray("Tiffin");
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject=jsonArray.getJSONObject(i);

                        Tiffin_ID.add(jsonObject.getString("Tiffin_ID"));
                        Sabji.add(jsonObject.getString("Sabji"));
                        Dal.add(jsonObject.getString("Dal"));
                        Roti.add(jsonObject.getString("Roti"));
                        Paratha.add(jsonObject.getString("Paratha"));
                        Rice.add(jsonObject.getString("Rice"));
                        Papad.add(jsonObject.getString("Papad"));
                        Pickel.add(jsonObject.getString("Pickel"));
                        Dessert.add(jsonObject.getString("Dessert"));
                        Price.add(jsonObject.getString("Price"));
                        Salad.add(jsonObject.getString("Salad"));
                        Tiffin_Type_list.add(jsonObject.getString("Tiffin_Type"));
                        Tiffin_Type.put(jsonObject.getString("Tiffin_Type"),i);

                    }

                }


                else {

                    String success2 = json.getString("success");
                    Log.d("success", success2);

                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String result) {

            txt_mini_price.setText(Price.get(0)+" Rs.");
            txt_avg_price.setText(Price.get(1)+" Rs.");
            txt_full_price.setText(Price.get(2)+" Rs.");

            super.onPostExecute(result);

        }
    }
}
