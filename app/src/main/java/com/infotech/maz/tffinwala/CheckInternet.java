package com.infotech.maz.tffinwala;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by maz on 3/3/2017.
 */

public class CheckInternet {
    private static final String TAG=CheckInternet.class.getName();


    public static boolean IsInternetAvailable(Context context){

        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;


        NetworkInfo  info=(NetworkInfo)((ConnectivityManager)
                           context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();


       if(info!=null){

           if(info.getType()==ConnectivityManager.TYPE_WIFI)
           {
               haveConnectedWifi=true;
           }
          else if(info.getType()==ConnectivityManager.TYPE_MOBILE){
               haveConnectedMobile=true;
           }
       }
        else {


       }
        return haveConnectedMobile || haveConnectedWifi;

    }
}
