package com.infotech.maz.tffinwala;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ForgotPassword extends AppCompatActivity {

    EditText edt_email;
    Button btn_send;
    String email;
    int status=0;
    String OTP="";
    public static final String URL_FORGOT="http://tiffinwala.16mb.com/forgot_password.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        this.setTitle("Forgot Password");

        edt_email=(EditText)findViewById(R.id.activity_forgot_password_edt_email);
        btn_send=(Button)findViewById(R.id.activity_forgot_password_submit);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email=edt_email.getText().toString();

                if((email.indexOf("@")<1 || email.lastIndexOf(".")<email.indexOf("@")+2 || email.lastIndexOf(".")+2>=email.length())){
                    Toast.makeText(ForgotPassword.this, "Your Email ?", Toast.LENGTH_SHORT).show();
                }
                else {

                    Random rnd = new Random();
                    int n = 1000 + rnd.nextInt(9000);

                    OTP=String.valueOf(n);

                    new ForgotPasswordCheck().execute();
                }
            }
        });
    }

    class ForgotPasswordCheck extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        public String doInBackground(String... args) {
            // creating new product in background thread


            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();


                params.add(new BasicNameValuePair("Email", email));

                params.add(new BasicNameValuePair("OTP", OTP));


                JSONParser jParser =new JSONParser();

                JSONObject json = jParser.makeHttpRequest(URL_FORGOT, "POST", params);


                int success1 = Integer.parseInt(json.getString("success"));
                Log.e("data", params.toString());
                Log.d("success", json.toString());

                if (success1 == 1) {

                    //    Toast.makeText(Complete_Profile.this, "Registration Successfully..!!", Toast.LENGTH_SHORT).show();
                    //    startActivity(new Intent(Complete_Profile.this,LoginActivity.class));
                      status=1;

                }

                if(success1==0)
                {
                    status=0;
                }


                else {
                    String success2 = json.getString("success");
                    Log.d("success", success2);
                  status=2;
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String result) {
            if(status==1){
                Intent i =new Intent(ForgotPassword.this,SetOtp.class);
                i.putExtra("OTP",OTP);
                i.putExtra("Email",email);
                startActivity(i);
            }
            else if(status==0)
            {
                Toast.makeText(ForgotPassword.this, "Mail Id Not Registred With Tiffinwala", Toast.LENGTH_SHORT).show();
              startActivity(new Intent(ForgotPassword.this,SignUp.class));

            }
            else {
                Toast.makeText(ForgotPassword.this, "OTP not sent Try Again.!!!", Toast.LENGTH_SHORT).show();
            }

            super.onPostExecute(result);

        }
    }
}
