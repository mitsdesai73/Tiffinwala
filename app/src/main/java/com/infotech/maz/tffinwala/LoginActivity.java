package com.infotech.maz.tffinwala;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener{

    boolean pass=false;
    EditText edt_pass,edt_email;
    TextView txt_signup,txt_forgot_pass;
    Button btn_login;
    String mobile="";
    private GoogleApiClient googleApiClient;
    private SignInButton signInButton;
    JSONParser jParser=new JSONParser();
    String email;
    String password;
    boolean flag=false;
    int login_status=0;
    LinearLayout lin_login;
    private SharedPreferences pref_login;
    SharedPreferences.Editor editor_login;
    AVLoadingIndicatorView loadingIndicatorView;

    private static final int RC_SIGN_IN = 007;
    private static final String TAG = LoginActivity.class.getSimpleName();
    public static final String URL_LOGIN="http://tiffinwala.16mb.com/login.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.setTitle("Login");

        loadingIndicatorView=(AVLoadingIndicatorView)findViewById(R.id.avi);

        pref_login=getSharedPreferences("Login",MODE_PRIVATE);
        editor_login=pref_login.edit();
        edt_pass=(EditText)findViewById(R.id.activity_login_edt_password);
        txt_signup=(TextView)findViewById(R.id.activity_login_txt_signup);
        edt_email=(EditText)findViewById(R.id.activity_login_edt_email);
        btn_login=(Button)findViewById(R.id.activity_login_btn_login);
        txt_forgot_pass=(TextView)findViewById(R.id.activity_login_txt_forgot_password);
        signInButton=(SignInButton)findViewById(R.id.activity_login_btn_sign_in);

        lin_login=(LinearLayout)findViewById(R.id.activity_lin_login);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        signInButton.setScopes(gso.getScopeArray());

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });

txt_forgot_pass.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        startActivity(new Intent(LoginActivity.this,ForgotPassword.class));
    }
});

        txt_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(LoginActivity.this,SignUp.class);
                startActivity(i);
            }
        });


        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email=edt_email.getText().toString();
                password=edt_pass.getText().toString();
                if(!(email.indexOf("@")<1 || email.lastIndexOf(".")<email.indexOf("@")+2 || email.lastIndexOf(".")+2>=email.length())){
                    if(password.length()>0){

                        email=edt_email.getText().toString();
                        password=edt_pass.getText().toString();

                        new CheckUser().execute();

                    }
                    else {
                        edt_pass.setError("Your Password ?");
                    }
                }
                else {

                    edt_email.setError("Enter Valid Email ");
                   // Toast.makeText(LoginActivity.this, "Is This Your Valid Email ?", Toast.LENGTH_SHORT).show();
                }
            }
        });

        edt_pass.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(edt_pass.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD)
                {
                    final int DRAWABLE_LEFT = 0;
                    final int DRAWABLE_TOP = 1;
                    final int DRAWABLE_RIGHT = 2;
                    final int DRAWABLE_BOTTOM = 3;

                    if(event.getAction()==MotionEvent.ACTION_UP){
                        if(event.getRawX() >= (edt_pass.getRight() - edt_pass.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())){


                            edt_pass.setCompoundDrawablesWithIntrinsicBounds(R.drawable.user_passwor_24, 0, R.drawable.close_eye_pass, 0);
                            edt_pass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        }
                    }

                }
                else {
                    final int DRAWABLE_LEFT = 0;
                    final int DRAWABLE_TOP = 1;
                    final int DRAWABLE_RIGHT = 2;
                    final int DRAWABLE_BOTTOM = 3;

                    if(event.getAction()==MotionEvent.ACTION_UP){
                        if(event.getRawX() >= (edt_pass.getRight() - edt_pass.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())){


                            edt_pass.setCompoundDrawablesWithIntrinsicBounds(R.drawable.user_passwor_24, 0, R.drawable.open_eye_pass, 0);
                            edt_pass.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                        }
                    }
                }



                edt_pass.setSelection(edt_pass.getText().length());

                return false;
            }
        });
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }



    private void handleSignInResult(GoogleSignInResult result) {
      //  Log.e(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
        String G_personName = acct.getDisplayName();
        String G_email = acct.getEmail();
            editor_login.putString("LogedIn_User_G",G_email);
            editor_login.commit();
            startActivity(new Intent(LoginActivity.this,Welcome.class));
       } else {

            Toast.makeText(this, "Login Failed, Try Again..!!", Toast.LENGTH_SHORT).show();
        }
    }

 /*   private void getMobileNumber() {

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Your Mobile ?");
        //   alert.setMessage("Message");

        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                if(input.getText().toString().length()!=10)

                {
                    getMobileNumber();
                }

            }
        });


        alert.show();





    }*/

    @Override
    public void onBackPressed() {

        finish();
        moveTaskToBack(true);


        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==RC_SIGN_IN){
            GoogleSignInResult googleSignInResult= Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(googleSignInResult);
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    class CheckUser extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
          //  lin_login.setVisibility(View.INVISIBLE);
          //  loadingIndicatorView.setVisibility(View.VISIBLE);
//loadingIndicatorView.show();
            super.onPreExecute();
        }

        /**
         * getting All products from url
         */
        @Override
        public String doInBackground(String... args) {
            // creating new product in background thread


            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();


                params.add(new BasicNameValuePair("Email", email));
                params.add(new BasicNameValuePair("Password", password));


                JSONObject json = jParser.makeHttpRequest(URL_LOGIN, "POST", params);


                int success1 = Integer.parseInt(json.getString("success"));
                Log.e("data", params.toString());
                Log.d("success", json.toString());
                if (success1 == 1) {

                       login_status=1;

                }

                else if (success1 == 0) {

                    login_status=0;

                }

                else if(success1==2)
                {
                    login_status=2;
                }

                else if(success1==3)
                {
                    login_status=3;
                }
                else {

                    String success2 = json.getString("success");
                    Log.d("success", success2);

                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String result) {
           if(login_status==1){
         //      loadingIndicatorView.hide();
         //      loadingIndicatorView.setVisibility(View.INVISIBLE);
               editor_login.putString("LogedIn_User",email);
               editor_login.commit();
               startActivity(new Intent(LoginActivity.this,Welcome.class));
               Toast.makeText(LoginActivity.this, "Login Success..!!", Toast.LENGTH_SHORT).show();
           }
           else if(login_status==2){
               lin_login.setVisibility(View.VISIBLE);
               loadingIndicatorView.hide();
               loadingIndicatorView.setVisibility(View.INVISIBLE);
                Toast.makeText(LoginActivity.this, "Check Your Email ?", Toast.LENGTH_SHORT).show();
            }
           else if(login_status==3){
               lin_login.setVisibility(View.VISIBLE);
               loadingIndicatorView.hide();
               loadingIndicatorView.setVisibility(View.INVISIBLE);
                Toast.makeText(LoginActivity.this, "Try Again", Toast.LENGTH_SHORT).show();
            }
            else if(login_status==0){
               lin_login.setVisibility(View.VISIBLE);
               loadingIndicatorView.hide();
               loadingIndicatorView.setVisibility(View.INVISIBLE);
                Toast.makeText(LoginActivity.this, "Email Password Not Match!!", Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(result);

        }
    }
}
