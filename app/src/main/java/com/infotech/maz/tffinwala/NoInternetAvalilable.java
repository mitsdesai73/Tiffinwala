package com.infotech.maz.tffinwala;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;

public class NoInternetAvalilable extends AppCompatActivity {

    ImageView img_elephant;
    SwipeRefreshLayout refreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_internet_avalilable);

img_elephant=(ImageView)findViewById(R.id.activity_no_internet_avalilable_img_elephant);
        refreshLayout=(SwipeRefreshLayout)findViewById(R.id.activity_no_internet_avalilable_refresh);

    /*    refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
              if(MainActivity.)
              {
                  startActivity(new Intent(NoInternetAvalilable.this,MainActivity.class));
              }
                else {
                  refreshLayout.setRefreshing(false);
              }
            }
        });
*/
        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(img_elephant);
        Glide.with(getApplicationContext()).load(R.drawable.no_internet_avalable).into(imageViewTarget);
    }

}
