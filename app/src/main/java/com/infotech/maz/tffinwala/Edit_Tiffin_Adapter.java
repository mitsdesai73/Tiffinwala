package com.infotech.maz.tffinwala;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mits on 01-04-2017.
 */
public class Edit_Tiffin_Adapter extends RecyclerView.Adapter<Edit_Tiffin_Adapter.MyViewHolder> {
    Context context;
    LayoutInflater inflater;
    String date;
    String order_id;
    int flag=0;
    int indx=0;
    public static final String URL_ADD_CANCEL_DATE="http://tiffinwala.16mb.com/add_cancel_date.php";
    JSONParser jParser =new JSONParser();
    ArrayList<Model_Edit_Tiffin_Order_Details> tiffin_details=new ArrayList<Model_Edit_Tiffin_Order_Details>();



    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tiffin_type,days;
        ImageView img_cancel;
        public MyViewHolder(View itemView) {
            super(itemView);
            tiffin_type=(TextView)itemView.findViewById(R.id.edit_tiffin_view_txt_tiffintype);
            days=(TextView)itemView.findViewById(R.id.edit_tiffin_view_txt_No_of_days);
            img_cancel=(ImageView) itemView.findViewById(R.id.edit_tiffin_view_img_cancel);
        }
    }

    public Edit_Tiffin_Adapter(Context context, ArrayList<Model_Edit_Tiffin_Order_Details> tiffin_detail, String date){
        this.context=context;
        this.tiffin_details=tiffin_detail;
        this.date=date;
        inflater=(LayoutInflater.from(context));
    }


    @Override
    public Edit_Tiffin_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.edit_tiffin_view,parent,false);
       // Model_Cancel_Tiffin_Order_Details orderDetails=tiffin_details.get(po)
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Edit_Tiffin_Adapter.MyViewHolder holder, final int position) {

        holder.tiffin_type.setText(tiffin_details.get(position).getTiffin_Type());
        int No_of_Days=0;

        if(!tiffin_details.get(position).getDay().toString().equals("0")){
            No_of_Days= Integer.parseInt(tiffin_details.get(position).getDay().toString());
        }
        else if(!tiffin_details.get(position).getWeek().toString().equals("0")) {
            No_of_Days=Integer.parseInt(tiffin_details.get(position).getWeek().toString())*7;
        }
        else if(!tiffin_details.get(position).getMonth().toString().equals("0")) {
            No_of_Days=Integer.parseInt(tiffin_details.get(position).getMonth().toString())*30;
        }
        holder.days.setText(String.valueOf(No_of_Days));

        holder.img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  Toast.makeText(context, "Order ID"+tiffin_details.get(position).getOrder_ID().toString(), Toast.LENGTH_SHORT).show();
                order_id=tiffin_details.get(position).getOrder_ID().toString();
                indx=position;
                date=Edit_Tiffin.Today_date;

                Intent intent=new Intent(context,Update_Tiffin.class);
               intent.putExtra("Order_ID",tiffin_details.get(position).getOrder_ID().toString());
                intent.putExtra("Tiffin_Type",tiffin_details.get(position).getTiffin_Type().toString());
                intent.putExtra("Lunch",tiffin_details.get(position).getLunch().toString());
                intent.putExtra("Dinner",tiffin_details.get(position).getDinner().toString());
                intent.putExtra("Day",tiffin_details.get(position).getDay().toString());
                intent.putExtra("Week",tiffin_details.get(position).getWeek().toString());
                intent.putExtra("Month",tiffin_details.get(position).getMonth().toString());
                intent.putExtra("Tiffin_Count",tiffin_details.get(position).getTiffin_Count().toString());
                intent.putExtra("Start_Date",tiffin_details.get(position).getStart_Date().toString());
                intent.putExtra("End_Date",tiffin_details.get(position).getEnd_Date().toString());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
               // new AddCancelDate().execute();

            }
        });
    }

    @Override
    public int getItemCount() {
        return tiffin_details.size();
    }



    class AddCancelDate extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        /**
         * getting All products from url
         */
        @Override
        public String doInBackground(String... args) {
            // creating new product in background thread


            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();


                params.add(new BasicNameValuePair("Order_ID",order_id));
                params.add(new BasicNameValuePair("Date",date));


                JSONObject json = jParser.makeHttpRequest(URL_ADD_CANCEL_DATE, "POST", params);


                int success1 = Integer.parseInt(json.getString("success"));
                Log.e("data", params.toString());
                Log.d("success", json.toString());
                if (success1 == 1) {

                        flag=1;

                    }
                else if(success1==3){
                    flag=3;
                }




                else {

                    String success2 = json.getString("success");
                    Log.d("success", success2);

                    flag=0;

                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String result) {

            if(flag==1)
            {
                tiffin_details.remove(indx);
                Cancel_Tiffin.adapter.notifyDataSetChanged();
            }
            else if(flag==3){
                Toast.makeText(context, "Select Valid Date", Toast.LENGTH_SHORT).show();
            }

            super.onPostExecute(result);

        }
    }
}
