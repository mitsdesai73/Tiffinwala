package com.infotech.maz.tffinwala;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity  implements ConnectivityReceiver.ConnectivityReceiverListener  {

    SharedPreferences pref_login;
    SharedPreferences.Editor editor_login;
    String Email="";

    ConnectivityReceiver.ConnectivityReceiverListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pref_login=getSharedPreferences("Login",MODE_PRIVATE);
        editor_login=pref_login.edit();


       Email=pref_login.getString("LogedIn_User","");



if(checkConnection()){
    if(Email.length()>0)
    {
        startActivity(new Intent(MainActivity.this,Welcome .class));
    }
    else {
        startActivity(new Intent(MainActivity.this,LoginActivity.class));
    }


}
        else {
    startActivity(new Intent(MainActivity.this,NoInternetAvalilable.class));
        }
      //  checkConnection();


    }



  /*  private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        if(isConnected){
            startActivity(new Intent(MainActivity.this,LoginActivity.class));
        }
        else {
            startActivity(new Intent(MainActivity.this,NoInternetAvalilable.class));
        }

    }*/


    /*  @Override
      public boolean onKeyDown(int keyCode, KeyEvent event) {
          if(event.getAction() == KeyEvent.ACTION_DOWN){
              switch(keyCode)
              {
                  case KeyEvent.KEYCODE_BACK:
                      if(mWebView.canGoBack() == true){
                          mWebView.goBack();
                      }else{
                          finish();
                      }
                      return true;
              }
  
          }
          return super.onKeyDown(keyCode, event);
      }*/
/*  protected void onResume() {
      super.onResume();

      // register connection status listener
      MyApplication.getInstance().setConnectivityListener(this);
  }
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }*/

    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected(getApplicationContext());
        return isConnected;
    }






   @Override
    public void onNetworkConnectionChanged(boolean isConnected) {


       if(isConnected)
       {
           startActivity(new Intent(MainActivity.this,MainActivity.class));
       }
       else {
           startActivity(new Intent(MainActivity.this,NoInternetAvalilable.class));
       }
    }

}
