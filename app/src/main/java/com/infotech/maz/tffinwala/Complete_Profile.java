package com.infotech.maz.tffinwala;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Complete_Profile extends AppCompatActivity {

    Button btn_selectMealDetails;
    EditText edt_locality,edt_block;
    private static final String fetch_state="http://tiffinwala.16mb.com/fetch_state.php";
    private static final String add_user="http://tiffinwala.16mb.com/add_user.php";
    private static final String fetch_city="http://tiffinwala.16mb.com/fetch_city.php";
    AutoCompleteTextView spn_city;
    AutoCompleteTextView spn_state;
    String block;
    String locality;
    HashMap<String,String> city_map=new HashMap<String ,String >();
    ArrayList<String> city_name = new ArrayList<String>();
    ArrayList<String> state_name = new ArrayList<String>();

    SharedPreferences pref_login;
    SharedPreferences.Editor editor_login;

    boolean flag=true;
    boolean flag1=false;
    String abc="Abc";
    HashMap<String,Integer> map_state=new HashMap<String, Integer>();
    String selected_city="";
    String selected_state="";
    String selected_city_ID="";
    String Email;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete__profile);

        this.setTitle("Address");

        pref_login=getSharedPreferences("Login",MODE_PRIVATE);
        Email=pref_login.getString("LogedIn_User",null);
        editor_login=pref_login.edit();

        btn_selectMealDetails=(Button)findViewById(R.id.signUp_btn_submit);
        edt_block=(EditText)findViewById(R.id.activity_complete_profile_edt_block);
        edt_locality=(EditText)findViewById(R.id.activity_complete_profile_edt_locality);
        btn_selectMealDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                block=edt_block.getText().toString();
                locality=edt_locality.getText().toString();

                    if (block.length() != 0) {
                        if (locality.length() > 3) {
                            if (selected_city.length() != 0) {
                                if (selected_state.length() != 0) {

                                   new RegisterUser().execute();
                                    //startActivity(new Intent(Complete_Profile.this,TiffinSelection.class));
                                } else {
                                    Toast.makeText(Complete_Profile.this, " Your State ?", Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(Complete_Profile.this, " Your City ?", Toast.LENGTH_SHORT).show();

                            }
                        } else {
                            Toast.makeText(Complete_Profile.this, " Your Locality  ?", Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        Toast.makeText(Complete_Profile.this, "Your Block/House No ?", Toast.LENGTH_SHORT).show();
                    }



            }
        });

       new State().execute();





        spn_state = (AutoCompleteTextView) findViewById(R.id.activity_complete_profile_spn_state);
        spn_city = (AutoCompleteTextView) findViewById(R.id.activity_complete_profile_spn_city);





     final ArrayAdapter  arrayAdapter_state = new ArrayAdapter(this, R.layout.auto_complete_text_view_row, state_name);
        arrayAdapter_state.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_state.setAdapter(arrayAdapter_state);




        spn_state.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selected_state=(String) parent.getItemAtPosition(position);
                spn_city.setText("");
                city_name.clear();
                new City().execute();

                    ArrayAdapter arrayAdapter_city = new ArrayAdapter(getApplicationContext(), R.layout.auto_complete_text_view_row, city_name);
                    arrayAdapter_city.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spn_city.setAdapter(arrayAdapter_city);



            }
        });

        spn_city.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selected_city=(String) parent.getItemAtPosition(position);
                selected_city_ID=city_map.get(selected_city);

               Toast.makeText(Complete_Profile.this, "City"+selected_city_ID, Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public void onBackPressed() {

        startActivity(new Intent(Complete_Profile.this,Complete_Profile.class));
        super.onBackPressed();
    }

   class State extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        public String doInBackground(String... args) {
            // creating new product in background thread


            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();


                params.add(new BasicNameValuePair("mobile",abc));


                JSONParser jParser=new JSONParser();


                JSONObject json = jParser.makeHttpRequest(fetch_state, "POST", params);


                int success1 = Integer.parseInt(json.getString("success"));
                Log.e("data", params.toString());
                Log.d("success", json.toString());
                if (success1 == 1) {

                    JSONArray ownerObj = json.getJSONArray("State");

                    for(int i=0;i<ownerObj.length();i++){

                        JSONObject json_state=ownerObj.getJSONObject(i);

                        state_name.add(json_state.getString("State_Name"));

                    }



                }

                else {
                    String success2 = json.getString("success");
                    Log.d("success", success2);

                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

        }
    }


    class City extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {

         //   super.onPreExecute();

        }

        @Override
        public String doInBackground(String... args) {
            // creating new product in background thread


            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();


                params.add(new BasicNameValuePair("State_Name",selected_state));

                JSONParser jParser=new JSONParser();

                JSONObject json = jParser.makeHttpRequest(fetch_city, "POST", params);


                int success1 = Integer.parseInt(json.getString("success"));
                Log.e("data", params.toString());
                Log.d("success", json.toString());
                if (success1 == 1) {

                    JSONArray ownerObj = json.getJSONArray("City");

                    for(int i=0;i<ownerObj.length();i++){

                        JSONObject json_state=ownerObj.getJSONObject(i);

                        city_name.add(json_state.getString("City_Name"));
                        city_map.put(json_state.getString("City_Name"),json_state.getString("City_ID"));

                    }

                    flag=true;



                }
                else {
                    String success2 = json.getString("success");
                    Log.d("success", success2);

                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(String result) {
          //  super.onPostExecute(result);


        }
    }



    class RegisterUser extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        /**
         * getting All products from url
         */
        @Override
        public String doInBackground(String... args) {
            // creating new product in background thread


            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();


                params.add(new BasicNameValuePair("Email", Email));
                params.add(new BasicNameValuePair("Block", block));
                params.add(new BasicNameValuePair("Locality", locality));
                params.add(new BasicNameValuePair("City_ID", selected_city_ID));

                JSONParser jParser =new JSONParser();

                JSONObject json = jParser.makeHttpRequest(add_user, "POST", params);


                int success1 = Integer.parseInt(json.getString("success"));
                Log.e("data", params.toString());
                Log.d("success", json.toString());
                if (success1 == 1) {

               flag1=true;

                }

                else {
                    flag1=false;
                    String success2 = json.getString("success");
                    Log.d("success", success2);

                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String result) {

           if(flag1)
           {
               Toast.makeText(Complete_Profile.this, "Register Successfully..!!", Toast.LENGTH_SHORT).show();
               startActivity(new Intent(Complete_Profile.this,Welcome.class));
           }
            else {
               Toast.makeText(Complete_Profile.this, "Register Again..!!", Toast.LENGTH_SHORT).show();

           }
            super.onPostExecute(result);

        }
    }


}
