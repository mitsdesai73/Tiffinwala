package com.infotech.maz.tffinwala;

import android.content.Context;

/**
 * Created by Mits on 01-04-2017.
 */
public class Model_Edit_Tiffin_Order_Details {
    String Order_ID;
    String Tiffin_Type;
    String Lunch;
    String Dinner;
    String Day;
    String Week;
    String Month;
    String Tiffin_Count;
    String Start_Date;
    String End_Date;



    Context context;

   public Model_Edit_Tiffin_Order_Details(Context context, String Order_ID, String Tiffin_Type, String Lunch, String Dinner, String Day, String Week, String Month, String Tiffin_Count, String Start_Date,String End_Date){
        this.context=context;
        this.Order_ID=Order_ID;
        this.Tiffin_Type=Tiffin_Type;
       this.Lunch= Lunch;
       this.Dinner= Dinner;
       this.Day= Day;
       this.Week= Week;
       this.Month= Month;
       this.Tiffin_Count= Tiffin_Count;
       this.Start_Date=Start_Date;
       this.End_Date=End_Date;

   }

    public String getEnd_Date() {
        return End_Date;
    }

    public void setEnd_Date(String end_Date) {
        End_Date = end_Date;
    }

    public String getOrder_ID() {
        return Order_ID;
    }

    public void setOrder_ID(String order_ID) {
        Order_ID = order_ID;
    }

    public String getTiffin_Type() {
        return Tiffin_Type;
    }

    public void setTiffin_Type(String tiffin_Type) {
        Tiffin_Type = tiffin_Type;
    }

    public String getLunch() {
        return Lunch;
    }

    public void setLunch(String lunch) {
        Lunch = lunch;
    }

    public String getDinner() {
        return Dinner;
    }

    public void setDinner(String dinner) {
        Dinner = dinner;
    }

    public String getDay() {
        return Day;
    }

    public void setDay(String day) {
        Day = day;
    }

    public String getWeek() {
        return Week;
    }

    public void setWeek(String week) {
        Week = week;
    }

    public String getMonth() {
        return Month;
    }

    public void setMonth(String month) {
        Month = month;
    }

    public String getTiffin_Count() {
        return Tiffin_Count;
    }

    public void setTiffin_Count(String tiffin_Count) {
        Tiffin_Count = tiffin_Count;
    }

    public String getStart_Date() {
        return Start_Date;
    }

    public void setStart_Date(String start_Date) {
        Start_Date = start_Date;
    }



}
