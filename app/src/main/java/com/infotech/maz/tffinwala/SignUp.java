package com.infotech.maz.tffinwala;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SignUp extends AppCompatActivity {
    EditText edt_name,edt_email,edt_mobile,edt_password,edt_confoPass;
    private Button btn_signUp;
    String name,email,mobile,password,confoPassword;
    int registration_status=0;
    SharedPreferences pref_login;

    SharedPreferences.Editor editor_login;
    private static final String check_user="http://tiffinwala.16mb.com/user_registraton.php";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

       // getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        this.setTitle("Sign Up");

        pref_login=getSharedPreferences("Login",MODE_PRIVATE);
        editor_login=pref_login.edit();

        edt_name=(EditText)findViewById(R.id.activity_sign_up_edt_username);
        edt_email=(EditText)findViewById(R.id.activity_sign_up_edt_email);
        edt_mobile=(EditText)findViewById(R.id.activity_sign_up_edt_mobile);
        edt_password=(EditText)findViewById(R.id.activity_sign_up_edt_password);
        edt_confoPass=(EditText)findViewById(R.id.activity_sign_up_edt_confoPassword);



        btn_signUp=(Button)findViewById(R.id.signUp_btn_submit);

        btn_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                boolean data_valid=true;


                        name=edt_name.getText().toString();
                        email=edt_email.getText().toString();
                        mobile=edt_mobile.getText().toString();
                        password=edt_password.getText().toString();
                        confoPassword=edt_confoPass.getText().toString();

                if(name.length()==0 || name==null)
                {
                    edt_name.setError("Your Name ? ");
                    data_valid=false;
                }
                else {
                    if(!(email.indexOf("@")<1 || email.lastIndexOf(".")<email.indexOf("@")+2 || email.lastIndexOf(".")+2>=email.length()))
                    {

                        if(mobile.length()==0 || mobile==null){
                            edt_mobile.setError("Your Mobile Number ?");
                            data_valid=false;
                        }else {
                            if(password.length()==0 || password==null){
                                edt_password.setError("Your Password ?");
                                data_valid=false;
                            }
                            else {
                                if(confoPassword.length()==0 || confoPassword==null){
                                    edt_confoPass.setError("Your Conform Password ?");
                                    data_valid=false;
                                }
                                else {
                                    if(!password.equals(confoPassword))
                                    {
                                        edt_confoPass.setError("Password Not Match");
                                        data_valid=false;
                                    }
                                    else {

                                        if(data_valid)
                                        {
                                            new RegisterUserCheck().execute();
                                        }
                                        else {
                                            Toast.makeText(SignUp.this, "Enter Your Valid Details", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                }

                            }
                        }




                    }
                    else {

                        edt_email.setError("Valid Email ? ");
                        data_valid=false;
                    }

                }

            }
        });
    }

    @Override
    public void onBackPressed() {

        startActivity(new Intent(SignUp.this,LoginActivity.class));

        super.onBackPressed();
    }

    class RegisterUserCheck extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        /**
         * getting All products from url
         */
        @Override
        public String doInBackground(String... args) {
            // creating new product in background thread


            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();


                params.add(new BasicNameValuePair("Email", email));
                params.add(new BasicNameValuePair("Mobile", mobile));
                params.add(new BasicNameValuePair("Name", name));
                params.add(new BasicNameValuePair("Password", password));


                JSONParser jParser =new JSONParser();

                JSONObject json = jParser.makeHttpRequest(check_user, "POST", params);


                int success1 = Integer.parseInt(json.getString("success"));
                Log.e("data", params.toString());
                Log.d("success", json.toString());

                if (success1 == 1) {
                    registration_status=1;
                    //    Toast.makeText(Complete_Profile.this, "Registration Successfully..!!", Toast.LENGTH_SHORT).show();
                    //    startActivity(new Intent(Complete_Profile.this,LoginActivity.class));


                }

                else   if (success1 == 2) {

                    registration_status=2;

                    //    Toast.makeText(Complete_Profile.this, "Error While Registration..!!", Toast.LENGTH_SHORT).show();
                    //     startActivity(new Intent(Complete_Profile.this,LoginActivity.class));


                }

                else if (success1 == 3) {

                    registration_status=3;

                    //     Toast.makeText(Complete_Profile.this, "Email And Mobile Already Registered..!!", Toast.LENGTH_SHORT).show();
                    //     startActivity(new Intent(Complete_Profile.this,LoginActivity.class));


                }

                else if (success1 == 4) {

                    registration_status=4;

                    //   Toast.makeText(Complete_Profile.this, "Email Already Registered..!!", Toast.LENGTH_SHORT).show();
                    //    startActivity(new Intent(Complete_Profile.this,LoginActivity.class));


                }

                else if (success1 == 5) {

                    registration_status=5;

           /*         Toast.makeText(Complete_Profile.this, "Mobile Already Registered..!!", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(Complete_Profile.this,LoginActivity.class));
*/

                }






                else {
                    String success2 = json.getString("success");
                    Log.d("success", success2);

                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }


        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String result) {

            if (registration_status == 1) {

                editor_login.putString("LogedIn_User",email);
                editor_login.commit();
                Toast.makeText(SignUp.this, "Complete Your Profile.!!", Toast.LENGTH_SHORT).show();
                Intent i=new Intent(SignUp.this,TiffinSelection.class);
                startActivity(i);
            }

            else   if (registration_status == 2) {



                Toast.makeText(SignUp.this, "Error While Registration..!!", Toast.LENGTH_SHORT).show();
                //     startActivity(new Intent(Complete_Profile.this,LoginActivity.class));
            }

            else if (registration_status == 3) {



                Toast.makeText(SignUp.this, "Email And Mobile Already Registered..!!", Toast.LENGTH_SHORT).show();
                //     startActivity(new Intent(Complete_Profile.this,LoginActivity.class));
            }

            else if (registration_status == 4) {



                Toast.makeText(SignUp.this, "Email Already Registered..!!", Toast.LENGTH_SHORT).show();
                //    startActivity(new Intent(Complete_Profile.this,LoginActivity.class));

            }

            else if (registration_status == 5) {



                Toast.makeText(SignUp.this, "Mobile Already Registered..!!", Toast.LENGTH_SHORT).show();
                //    startActivity(new Intent(Complete_Profile.this,LoginActivity.class));


            }


            else {

                Toast.makeText(SignUp.this, "Please Register Again..!!!", Toast.LENGTH_SHORT).show();
            }

            super.onPostExecute(result);

        }
    }
}
