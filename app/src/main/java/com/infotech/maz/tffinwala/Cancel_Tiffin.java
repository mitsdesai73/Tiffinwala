package com.infotech.maz.tffinwala;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.sundeepk.compactcalendarview.domain.Event;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Cancel_Tiffin extends AppCompatActivity {

    SharedPreferences pref_login;
    String Email;
    ImageView img_cancel_date;
   static String Cancel_date="";
    TextView txt_CancelDate;
    RecyclerView recyclerView;
    Calendar myCalendar = Calendar.getInstance();
    static Cancel_Tiffin_Adapter adapter;

    LinearLayout lin_cancel_tiffin;
    AVLoadingIndicatorView loadingIndicatorView;

   /* ArrayList<String> list_Order_ID=new ArrayList<String>();
    ArrayList<String> list_tiffin_type=new ArrayList<String>();
    ArrayList<String> list_Lunch=new ArrayList<String>();
    ArrayList<String> list_dinner=new ArrayList<String>();
    ArrayList<String> list_day=new ArrayList<String>();
    ArrayList<String> list_week=new ArrayList<String>();
    ArrayList<String> list_month=new ArrayList<String>();
    ArrayList<String> list_no_tiffin=new ArrayList<String>();
    ArrayList<String> list_start_date=new ArrayList<>();*/

    ArrayList<Model_Cancel_Tiffin_Order_Details> cancel_order_details=new ArrayList<Model_Cancel_Tiffin_Order_Details>();

    public static final String URL_Get_ORDER="http://tiffinwala.16mb.com/Cancel_tiffin.php";
    JSONParser jParser =new JSONParser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancel__tiffin);

        this.setTitle("Cancel Tiffin");

        lin_cancel_tiffin=(LinearLayout)findViewById(R.id.actvity_cancel_tiffin);
        loadingIndicatorView=(AVLoadingIndicatorView)findViewById(R.id.avi_cancel_tiffin);



        img_cancel_date=(ImageView)findViewById(R.id.activity_cancel_tiffin_img_cancel_date);
        txt_CancelDate=(TextView)findViewById(R.id.activity_cancel_tiffin_txt_cancel_date);
        recyclerView=(RecyclerView)findViewById(R.id.activity_cancel_tiffin_list_tiffin);

        pref_login=getSharedPreferences("Login",MODE_PRIVATE);
         adapter=new Cancel_Tiffin_Adapter(getApplicationContext(),cancel_order_details,Cancel_date);
        LinearLayoutManager manager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        String LogedIn_User=pref_login.getString("LogedIn_User",null);
        String LogedIn_User_G=pref_login.getString("LogedIn_User_G",null);

        if(LogedIn_User_G!=null)
        {
            Email=LogedIn_User_G;

        }
        else {
            Email=LogedIn_User;
        }

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();

                new FetchOrderTiffin().execute();


            }

        };

        img_cancel_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DatePickerDialog(Cancel_Tiffin.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();



            }
        });


    }

    private void updateLabel() {

        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        Cancel_date=sdf.format(myCalendar.getTime());
        txt_CancelDate.setText(sdf.format(myCalendar.getTime()));
      //  new FetchOrderTiffin().execute();
    }

    @Override

    public void onBackPressed() {
        startActivity(new Intent(Cancel_Tiffin.this,Welcome.class));
        super.onBackPressed();
    }

    class FetchOrderTiffin extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {

            lin_cancel_tiffin.setVisibility(View.INVISIBLE);
            loadingIndicatorView.show();

            super.onPreExecute();
        }

        /**
         * getting All products from url
         */
        @Override
        public String doInBackground(String... args) {
            // creating new product in background thread


            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();


                params.add(new BasicNameValuePair("Email",Email));
                params.add(new BasicNameValuePair("Date",Cancel_date));


                JSONObject json = jParser.makeHttpRequest(URL_Get_ORDER, "POST", params);


                int success1 = Integer.parseInt(json.getString("success"));
                Log.e("data", params.toString());
                Log.d("success", json.toString());
                if (success1 == 1) {

                    JSONArray jsonArray=json.getJSONArray("Order");
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject=jsonArray.getJSONObject(i);



                        String Order_ID=jsonObject.getString("Order_ID");
                        String Tiffin_Type=jsonObject.getString("Tiffin_Type");
                        String Lunch=jsonObject.getString("Lunch");
                        String Start_Date=jsonObject.getString("Start_Date");
                        String Dinner=jsonObject.getString("Dinner");
                        String Day=jsonObject.getString("Day");
                        String Week=jsonObject.getString("Week");
                        String Month=jsonObject.getString("Month");
                        String Tiffin_Count=jsonObject.getString("Tiffin_Count");

                        Model_Cancel_Tiffin_Order_Details model_cancel=new Model_Cancel_Tiffin_Order_Details(getApplicationContext(),Order_ID,Tiffin_Type,Lunch,Dinner,Day,Week, Month,Tiffin_Count,Start_Date);
                        cancel_order_details.add(model_cancel);



                    }

                }


                else {

                    String success2 = json.getString("success");
                    Log.d("success", success2);

                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/


        @Override
        protected void onPostExecute(String result) {

            adapter.notifyDataSetChanged();

/*


            for(int i=0;i<list_tiffin_type.size();i++){

                int NO_DAY=0;
                Calendar start = Calendar.getInstance();
                String Date=list_start_date.get(i);
                SimpleDateFormat formatter1=new SimpleDateFormat("yyyy-MM-dd");
                try {
                    java.util.Date date=formatter1.parse(Date);
                    start.setTime(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if(!list_day.get(i).equals("0"))
                {
                    NO_DAY=Integer.parseInt(list_day.get(i));
                }
                else if(!list_week.get(i).equals("0"))
                {
                    NO_DAY=Integer.parseInt(list_week.get(i))*7;
                }
                else if(!list_month.get(i).equals("0"))
                {
                    NO_DAY=Integer.parseInt(list_month.get(i))*30;
                }

                for(int j=0;j<NO_DAY;j++){

                    Date targetDay = start.getTime();
                    // Do Work Here

                    start.add(Calendar.DATE, 1);



                    Event ev2 = new Event(Color.CYAN,targetDay.getTime(),list_tiffin_type.get(i));
                    compactCalendarView.addEvent(ev2);
                }
            }


*/

            lin_cancel_tiffin.setVisibility(View.VISIBLE);
            loadingIndicatorView.hide();
            loadingIndicatorView.setVisibility(View.INVISIBLE);


            super.onPostExecute(result);

        }
    }

}
