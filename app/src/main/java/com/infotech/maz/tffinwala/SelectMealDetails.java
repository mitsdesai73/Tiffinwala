package com.infotech.maz.tffinwala;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class SelectMealDetails extends AppCompatActivity {

    LinearLayout lin_lunch,lin_dinner,lin_day,lin_week,lin_month,lin_1tiffin,lin_2tiffin,lin_3tiffin,lin_startDate,lin_duration;
    ImageView img_lunch,img_dinner,img_startDate;
   TextView txt_1tiffin,txt_2tiffin,txt_3tiffin,txt_duration;
    EditText edt_day,edt_week,edt_month;
    Calendar dateSelected = Calendar.getInstance();
    private DatePickerDialog datePickerDialog;
    Calendar myCalendar = Calendar.getInstance();
    String Meal_time_lunch="";
    String Meal_time_dinner="";
    FloatingActionButton fab_next;
    String No_of_tiffin="";
    String Start_date="";
    Boolean flag=false;
    public static final String Tiffin_order="http://tiffinwala.16mb.com/send_tiffin_order.php";
     ArrayList<String> selected_tiffin_list=new ArrayList<String>();
    ArrayList<String > selected_tiffin_ID=new ArrayList<String >();
    EditText edt_NoOfTiffin;
    TextView txt_startDate;
    SharedPreferences pref_tiffin;
    SharedPreferences pref_login;
    SharedPreferences.Editor editor_tiffin;
    SharedPreferences.Editor editor_login;
    String Email;
    String Day="";
    String Week="";
    String Month="";

    boolean Add_Order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_meal_details);
        this.setTitle("Meal Details");
        pref_tiffin = getSharedPreferences("Tiffin_Type", MODE_PRIVATE);
        pref_login=getSharedPreferences("Login",MODE_PRIVATE);

        editor_tiffin=pref_tiffin.edit();
        editor_login=pref_login.edit();

      Add_Order=pref_login.getBoolean("Add_Order",false);

            lin_lunch = (LinearLayout) findViewById(R.id.activity_select_meal_details_lin_lunch);
            lin_dinner = (LinearLayout) findViewById(R.id.activity_select_meal_details_lin_dinner);
            lin_day = (LinearLayout) findViewById(R.id.activity_select_meal_details_lin_day);
            lin_week = (LinearLayout) findViewById(R.id.activity_select_meal_details_lin_week);
            lin_month = (LinearLayout) findViewById(R.id.activity_select_meal_details_lin_month);
            lin_1tiffin = (LinearLayout) findViewById(R.id.activity_select_meal_details_lin_1tiffin);
            lin_2tiffin = (LinearLayout) findViewById(R.id.activity_select_meal_details_lin_2tiffin);
            lin_3tiffin = (LinearLayout) findViewById(R.id.activity_select_meal_details_lin_3tiffin);
            lin_startDate = (LinearLayout) findViewById(R.id.activity_select_meal_details_lin_startDate);
            lin_duration = (LinearLayout) findViewById(R.id.activity_select_meal_details_lin_duration);

            fab_next = (FloatingActionButton) findViewById(R.id.activity_select_meal_details_fab_next);

            img_lunch = (ImageView) findViewById(R.id.activity_select_meal_details_img_lunch);
            img_dinner = (ImageView) findViewById(R.id.activity_select_meal_details_img_dinner);
            img_startDate = (ImageView) findViewById(R.id.activity_select_meal_details_img_startDate);

            edt_day = (EditText) findViewById(R.id.activity_select_meal_details_edt_day);
            edt_week = (EditText) findViewById(R.id.activity_select_meal_details_edt_week);
            edt_month = (EditText) findViewById(R.id.activity_select_meal_details_edt_month);
            txt_1tiffin = (TextView) findViewById(R.id.activity_select_meal_details_txt_1tiffin);
            txt_2tiffin = (TextView) findViewById(R.id.activity_select_meal_details_txt_2tiffin);
            txt_3tiffin = (TextView) findViewById(R.id.activity_select_meal_details_txt_3tiffin);
            txt_duration = (TextView) findViewById(R.id.activity_select_meal_details_txt_duration);


            edt_NoOfTiffin = (EditText) findViewById(R.id.activity_select_meal_details_edt_NoOfTiffin);

            txt_startDate = (TextView) findViewById(R.id.activity_select_meal_details_txt_startDate);

         Email=pref_login.getString("LogedIn_User",null);


        Map<String, ?> allEntries = pref_tiffin.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            selected_tiffin_list.add(entry.getKey());
            selected_tiffin_ID.add(entry.getValue().toString());
        }
if(selected_tiffin_ID.size()>0)
{
    this.setTitle(selected_tiffin_list.get(0));
}

else {

        if(Add_Order) {
editor_login.remove("Add_Order");
            editor_login.apply();
            startActivity(new Intent(SelectMealDetails.this, Welcome.class));
        }
        else {
            startActivity(new Intent(SelectMealDetails.this, Complete_Profile.class));
        }
}


        fab_next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(Start_date.length()>0) {



                        if (selected_tiffin_ID.size() == 0) {


                            if(Add_Order){
                                editor_login.remove("Add_Order");
                                editor_login.apply();
                                startActivity(new Intent(SelectMealDetails.this, Welcome.class));
                            }else {
                                startActivity(new Intent(SelectMealDetails.this, Complete_Profile.class));
                            }
                        }



                        else {
                            new Tiffin_order_Details().execute();
                        }
                    }
                    else {
                        Toast.makeText(SelectMealDetails.this, "Start Date ?", Toast.LENGTH_SHORT).show();
                    }

                }
            });

            final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    // TODO Auto-generated method stub
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    updateLabel();
                }

            };


            img_lunch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lin_lunch.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBackLayout));

                    if (Meal_time_lunch.length() > 0 && Meal_time_dinner.length() > 0) {
                        lin_dinner.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBlack));
                        Meal_time_dinner = "";

                    }

                    Meal_time_lunch = "Lunch";
                    edt_day.setEnabled(true);
                    edt_month.setEnabled(true);
                    edt_week.setEnabled(true);


                }
            });

            img_dinner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lin_dinner.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBackLayout));

                    if (Meal_time_lunch.length() > 0 && Meal_time_dinner.length() > 0) {
                        lin_lunch.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBlack));
                        Meal_time_lunch = "";
                    }
                    Meal_time_dinner = "Dinner";
                    edt_day.setEnabled(true);
                    edt_month.setEnabled(true);
                    edt_week.setEnabled(true);

                }
            });


            lin_duration.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (Meal_time_lunch.length() != 0 || Meal_time_dinner.length() != 0) {

                        Toast.makeText(SelectMealDetails.this, "Your Meal Time ?", Toast.LENGTH_SHORT).show();
                    }

                    return false;
                }
            });


            txt_duration.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (Meal_time_lunch.length() != 0 || Meal_time_dinner.length() != 0) {

                        Toast.makeText(SelectMealDetails.this, "Your Meal Time ?", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            edt_day.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    if (Meal_time_lunch.length() != 0 || Meal_time_dinner.length() != 0) {

                        edt_month.setText("");
                        edt_week.setText("");

                        lin_day.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBackLayout));
                        lin_week.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBlack));
                        lin_month.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBlack));

                    } else {
                        Toast.makeText(SelectMealDetails.this, "Your Meal Time ?", Toast.LENGTH_SHORT).show();
                    }

                    return false;
                }
            });


            edt_week.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    if (Meal_time_lunch.length() != 0 || Meal_time_dinner.length() != 0) {

                        edt_month.setText("");
                        edt_day.setText("");

                        lin_day.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBlack));
                        lin_week.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBackLayout));
                        lin_month.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBlack));

                    } else {
                        Toast.makeText(SelectMealDetails.this, "Your Meal Time ?", Toast.LENGTH_SHORT).show();
                    }

                    return false;
                }
            });


            edt_month.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    if (Meal_time_lunch.length() != 0 || Meal_time_dinner.length() != 0) {

                        edt_day.setText("");
                        edt_week.setText("");

                        lin_day.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBlack));
                        lin_week.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBlack));
                        lin_month.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBackLayout));

                    } else {
                        Toast.makeText(SelectMealDetails.this, "Your Meal Time ?", Toast.LENGTH_SHORT).show();
                    }

                    return false;
                }
            });

            txt_1tiffin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Day=edt_day.getText().toString();
                    Week=edt_week.getText().toString();
                    Month=edt_month.getText().toString();

                    if (Day.length() > 0 || Week.length() > 0 || Month.length() > 0) {

                        No_of_tiffin = "1";

                        lin_1tiffin.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBackLayout));
                        lin_2tiffin.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBlack));
                        lin_3tiffin.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBlack));

                    } else {
                        Toast.makeText(SelectMealDetails.this, "Your Meal Duration ?", Toast.LENGTH_SHORT).show();
                    }

                }
            });

            txt_2tiffin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Day=edt_day.getText().toString();
                    Week=edt_week.getText().toString();
                    Month=edt_month.getText().toString();

                    if (Day.length() > 0 || Week.length() > 0 || Month.length() > 0) {

                        No_of_tiffin = "2";

                        lin_1tiffin.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBlack));
                        lin_2tiffin.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBackLayout));
                        lin_3tiffin.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBlack));
                    } else {
                        Toast.makeText(SelectMealDetails.this, "Your Meal Duration ?", Toast.LENGTH_SHORT).show();
                    }

                }
            });

            txt_3tiffin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Day=edt_day.getText().toString();
                    Week=edt_week.getText().toString();
                    Month=edt_month.getText().toString();

                    if (Day.length() > 0 || Week.length() > 0 || Month.length() > 0) {

                        No_of_tiffin = "3";

                        lin_1tiffin.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBlack));
                        lin_2tiffin.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBlack));
                        lin_3tiffin.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBackLayout));

                    } else {
                        Toast.makeText(SelectMealDetails.this, "Your Meal Duration ?", Toast.LENGTH_SHORT).show();
                    }

                }
            });

            edt_NoOfTiffin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Day=edt_day.getText().toString();
                    Week=edt_week.getText().toString();
                    Month=edt_month.getText().toString();

                    if (Day.length() > 0 || Week.length() > 0 || Month.length() > 0) {


                        lin_1tiffin.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBlack));
                        lin_2tiffin.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBlack));
                        lin_3tiffin.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBlack));

                    } else {
                        Toast.makeText(SelectMealDetails.this, "Your Meal Duration ?", Toast.LENGTH_SHORT).show();
                    }
                }
            });


            img_startDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (No_of_tiffin.length() != 0) {

                        new DatePickerDialog(SelectMealDetails.this, date, myCalendar
                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                myCalendar.get(Calendar.DAY_OF_MONTH)).show();

                        lin_startDate.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.ColorBackLayout));
                    } else {
                        Toast.makeText(SelectMealDetails.this, "Your No of Tiffin ?", Toast.LENGTH_SHORT).show();
                    }

                }
            });



        }


    @Override
    public void onBackPressed() {

        if(Add_Order){
            startActivity(new Intent(SelectMealDetails.this,TiffinSelection.class));
        }
        else {
            startActivity(new Intent(SelectMealDetails.this,SelectMealDetails.class));
        }


        super.onBackPressed();
    }


    private void updateLabel() {

        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        txt_startDate.setText(sdf.format(myCalendar.getTime()));
        Start_date=sdf.format(myCalendar.getTime());

    }



    class Tiffin_order_Details extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        /**
         * getting All products from url
         */
        @Override
        public String doInBackground(String... args) {
            // creating new product in background thread


            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();


                params.add(new BasicNameValuePair("Tiffin_ID", selected_tiffin_ID.get(0)));
                params.add(new BasicNameValuePair("Email", Email));
                if(Meal_time_lunch.length()==0){
                    params.add(new BasicNameValuePair("Lunch","0"));
                }
                else {
                    params.add(new BasicNameValuePair("Lunch","1"));
                }

                if(Meal_time_dinner.length()==0){
                    params.add(new BasicNameValuePair("Dinner","0"));
                }
                else {
                    params.add(new BasicNameValuePair("Dinner","1"));
                }

                if(Day.length()==0){
                    params.add(new BasicNameValuePair("Day","0"));
                }
                else {
                    params.add(new BasicNameValuePair("Day",Day));
                }

                if(Week.length()==0){
                    params.add(new BasicNameValuePair("Week","0"));
                }
                else {
                    params.add(new BasicNameValuePair("Week",Week));
                }
                if(Month.length()==0){
                    params.add(new BasicNameValuePair("Month","0"));
                }
                else {
                    params.add(new BasicNameValuePair("Month",Month));
                }

                params.add(new BasicNameValuePair("Tiffin_Count", No_of_tiffin));
                params.add(new BasicNameValuePair("Start_Date", Start_date));


                JSONParser jParser =new JSONParser();

                JSONObject json = jParser.makeHttpRequest(Tiffin_order, "POST", params);


                int success1 = Integer.parseInt(json.getString("success"));
                Log.e("data", params.toString());
                Log.d("success", json.toString());

                if (success1 == 1) {

                    flag=true;
                 //  registration_status=1;
                    //    Toast.makeText(Complete_Profile.this, "Registration Successfully..!!", Toast.LENGTH_SHORT).show();
                    //    startActivity(new Intent(Complete_Profile.this,LoginActivity.class));


                }




                else {

                    String success2 = json.getString("success");
                    Log.d("success", success2);

                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String result) {

            if (flag)
            {
                editor_tiffin.remove(selected_tiffin_list.get(0));
                editor_tiffin.commit();
                startActivity(new Intent(SelectMealDetails.this,SelectMealDetails.class));
            }


            super.onPostExecute(result);

        }
    }
}
