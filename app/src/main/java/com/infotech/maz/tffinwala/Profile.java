package com.infotech.maz.tffinwala;

import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.camera2.CameraManager;
import android.media.Image;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Profile extends AppCompatActivity {


    private EditText  edt_name,edt_email,edt_mobile;
    private EditText edt_block,edt_locality;
    private ImageView img_user_details,img_address_details,img_user_details_done,img_address_done;
    private AutoCompleteTextView txt_city,txt_state;

    private LinearLayout lin_profile;
    private AVLoadingIndicatorView loadingIndicatorView;

    String U_name="",U_email="",U_mobile="",U_block="",U_locality="",U_city="",U_State="";

    String User_ID="",State_ID="",City_ID="";

    boolean flag=false;


    public static final String URL_FETCH_USER="http://tiffinwala.16mb.com/fetch_user.php";
    public static final String URL_UPDATE_USER="http://tiffinwala.16mb.com/update_user_details.php";
    private static final String fetch_state="http://tiffinwala.16mb.com/fetch_state.php";
    private static final String add_user="http://tiffinwala.16mb.com/add_user.php";
    private static final String fetch_city="http://tiffinwala.16mb.com/fetch_city.php";

    String block;
    String locality;

    ArrayAdapter arrayAdapter_state;
    ArrayAdapter arrayAdapter_city;

    HashMap<String,String> city_map=new HashMap<String ,String >();
    ArrayList<String> city_name = new ArrayList<String>();
    ArrayList<String> state_name = new ArrayList<String>();

   boolean flag2=true;
    boolean flag1=false;
   String abc="Abc";
    HashMap<String,Integer> map_state=new HashMap<String, Integer>();
    String selected_city="";
    String selected_state="";
    String selected_city_ID="";

    SharedPreferences pref_login;
    String Email="";
    SharedPreferences.Editor editor_login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);


        pref_login=getSharedPreferences("Login",MODE_PRIVATE);

        Email=pref_login.getString("LogedIn_User",null);

        lin_profile=(LinearLayout)findViewById(R.id.activity_lin_profile);
        loadingIndicatorView=(AVLoadingIndicatorView)findViewById(R.id.avi_profile);


        new Fetch_User().execute();

        edt_name=(EditText)findViewById(R.id.activity_profile_edt_name);
        edt_email=(EditText)findViewById(R.id.activity_profile_edt_email);
        edt_mobile=(EditText)findViewById(R.id.activity_profile_edt_mobile);

        edt_block=(EditText) findViewById(R.id.activity_profile_edt_block);
        edt_locality=(EditText) findViewById(R.id.activity_profile_edt_locality);


       img_user_details=(ImageView)findViewById(R.id.activity_profile_img_user_details);
        img_address_details=(ImageView)findViewById(R.id.activity_profile_img_address_details);
        img_user_details_done=(ImageView)findViewById(R.id.activity_profile_img_user_details_done);
        img_address_done=(ImageView)findViewById(R.id.activity_profile_img_address_done) ;

        txt_city=(AutoCompleteTextView)findViewById(R.id.activity_profile_spn_city);
        txt_state=(AutoCompleteTextView)findViewById(R.id.activity_profile_spn_state);


        img_user_details_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                U_name=edt_name.getText().toString();
                U_mobile=edt_mobile.getText().toString();

               new  Update_Details().execute();

            }
        });

        img_user_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   edt_email.setEnabled(true);
                edt_mobile.setEnabled(true);
                edt_name.setEnabled(true);


            }
        });

        img_address_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                block=edt_block.getText().toString();
                locality=edt_locality.getText().toString();
                if(selected_city_ID.length()>0)
                {
                    new RegisterUser().execute();
                }
                else {
                    Toast.makeText(Profile.this, "Please Select City...!!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        img_address_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(Profile.this,Complete_Profile.class));

                state_name.clear();

                edt_block.setEnabled(true);
                edt_locality.setEnabled(true);
                txt_state.setEnabled(true);

                arrayAdapter_state = new ArrayAdapter(getApplicationContext(), R.layout.auto_complete_text_view_row, state_name);
                arrayAdapter_state.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                txt_state.setAdapter(arrayAdapter_state);




                txt_state.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        selected_state=(String) parent.getItemAtPosition(position);
                        txt_city.setText("");
                        city_name.clear();


                       arrayAdapter_city = new ArrayAdapter(getApplicationContext(), R.layout.auto_complete_text_view_row, city_name);
                        arrayAdapter_city.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        txt_city.setAdapter(arrayAdapter_city);

                        new City().execute();

                       txt_city.setEnabled(true);



                    }
                });

                txt_city.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        selected_city=(String) parent.getItemAtPosition(position);
                        selected_city_ID=city_map.get(selected_city);

                      //  Toast.makeText(Complete_Profile.this, "City"+selected_city_ID, Toast.LENGTH_SHORT).show();
                    }
                });


                new State().execute();
            }
        });

    }



    @Override
    public void onBackPressed() {

        startActivity(new Intent(Profile.this,Welcome.class));
        super.onBackPressed();
    }


    class Fetch_User extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {

            loadingIndicatorView.show();


            super.onPreExecute();
        }

        /**
         * getting All products from url
         */
        @Override
        public String doInBackground(String... args) {
            // creating new product in background thread


            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();

                params.add(new BasicNameValuePair("Email", Email));

                JSONParser jParser =new JSONParser();

                JSONObject json = jParser.makeHttpRequest(URL_FETCH_USER, "POST", params);


                int success1 = Integer.parseInt(json.getString("success"));
                Log.e("data", params.toString());
                Log.d("success", json.toString());

                if (success1 == 1) {

                    JSONArray jsonArray=json.getJSONArray("User");
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject=jsonArray.getJSONObject(i);

                        U_name=jsonObject.getString("User_Name");
                        U_mobile=jsonObject.getString("Mobile_Number");
                        U_email=jsonObject.getString("Email");
                        U_block=jsonObject.getString("Block");
                        U_locality=jsonObject.getString("Locality");
                        U_city=jsonObject.getString("City_Name");
                        U_State=jsonObject.getString("State_Name");

                        User_ID=jsonObject.getString("User_ID");
                        City_ID=jsonObject.getString("City_ID");
                        State_ID=jsonObject.getString("State_ID");






                    }

                }




                else {

                    String success2 = json.getString("success");
                    Log.d("success", success2);

                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String result) {

            setTitle(U_name);

            edt_name.setText(U_name);
            edt_mobile.setText(U_mobile);
            edt_email.setText(U_email);

            edt_block.setText(U_block);
            txt_state.setText(U_State);
            txt_city.setText(U_city);

            edt_locality.setText(U_locality);
            loadingIndicatorView.hide();
            loadingIndicatorView.setVisibility(View.INVISIBLE);
            lin_profile.setVisibility(View.VISIBLE);


            super.onPostExecute(result);

        }
    }

    class Update_Details extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        /**
         * getting All products from url
         */
        @Override
        public String doInBackground(String... args) {
            // creating new product in background thread


            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();

                params.add(new BasicNameValuePair("Email", Email));
                params.add(new BasicNameValuePair("Name", U_name));
                params.add(new BasicNameValuePair("Mobile", U_mobile));

                JSONParser jParser =new JSONParser();

                JSONObject json = jParser.makeHttpRequest(URL_UPDATE_USER, "POST", params);


                int success1 = Integer.parseInt(json.getString("success"));
                Log.e("data", params.toString());
                Log.d("success", json.toString());

                if (success1 == 1) {

                    flag=true;

                }




                else {

                    String success2 = json.getString("success");
                    Log.d("success", success2);

                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String result) {

            if(flag)
            {
                edt_name.setText(U_name);
                edt_mobile.setText(U_mobile);
                edt_name.setEnabled(false);
                edt_mobile.setEnabled(false);
                Toast.makeText(Profile.this, "User Details Updated Successfully..!!", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(Profile.this, "User Details Not Updated Successfully..!!", Toast.LENGTH_SHORT).show();
            }





            super.onPostExecute(result);

        }
    }



    class State extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        public String doInBackground(String... args) {
            // creating new product in background thread


            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();


                params.add(new BasicNameValuePair("mobile",abc));


                JSONParser jParser=new JSONParser();


                JSONObject json = jParser.makeHttpRequest(fetch_state, "POST", params);


                int success1 = Integer.parseInt(json.getString("success"));
                Log.e("data", params.toString());
                Log.d("success", json.toString());
                if (success1 == 1) {

                    JSONArray ownerObj = json.getJSONArray("State");

                    for(int i=0;i<ownerObj.length();i++){

                        JSONObject json_state=ownerObj.getJSONObject(i);

                        state_name.add(json_state.getString("State_Name"));

                    }



                }

                else {
                    String success2 = json.getString("success");
                    Log.d("success", success2);

                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(String result) {

            arrayAdapter_state.notifyDataSetChanged();

            super.onPostExecute(result);

        }
    }


    class City extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {

            //   super.onPreExecute();

        }

        @Override
        public String doInBackground(String... args) {
            // creating new product in background thread


            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();


                params.add(new BasicNameValuePair("State_Name",selected_state));

                JSONParser jParser=new JSONParser();

                JSONObject json = jParser.makeHttpRequest(fetch_city, "POST", params);


                int success1 = Integer.parseInt(json.getString("success"));
                Log.e("data", params.toString());
                Log.d("success", json.toString());
                if (success1 == 1) {

                    JSONArray ownerObj = json.getJSONArray("City");

                    for(int i=0;i<ownerObj.length();i++){

                        JSONObject json_state=ownerObj.getJSONObject(i);

                        city_name.add(json_state.getString("City_Name"));
                        city_map.put(json_state.getString("City_Name"),json_state.getString("City_ID"));

                    }

                    flag2=true;



                }
                else {
                    String success2 = json.getString("success");
                    Log.d("success", success2);

                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(String result) {

            arrayAdapter_city.notifyDataSetChanged();


             super.onPostExecute(result);


        }
    }



    class RegisterUser extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        public String doInBackground(String... args) {
            // creating new product in background thread


            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();


                params.add(new BasicNameValuePair("Email", Email));
                params.add(new BasicNameValuePair("Block", block));
                params.add(new BasicNameValuePair("Locality", locality));
                params.add(new BasicNameValuePair("City_ID", selected_city_ID));

                JSONParser jParser =new JSONParser();

                JSONObject json = jParser.makeHttpRequest(add_user, "POST", params);


                int success1 = Integer.parseInt(json.getString("success"));
                Log.e("data", params.toString());
                Log.d("success", json.toString());
                if (success1 == 1) {

                    flag1=true;

                }

                else {
                    flag1=false;
                    String success2 = json.getString("success");
                    Log.d("success", success2);

                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(String result) {

            if(flag1)
            {
                Toast.makeText(Profile.this, "Update Successfully..!!", Toast.LENGTH_SHORT).show();

               startActivity(new Intent(Profile.this,Profile.class));
            }
            else {
                Toast.makeText(Profile.this, "Try Again..!!", Toast.LENGTH_SHORT).show();

            }
            super.onPostExecute(result);

        }
    }


}
