package com.infotech.maz.tffinwala;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.EventLog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;

import com.github.sundeepk.compactcalendarview.domain.Event;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class Welcome extends AppCompatActivity implements  GoogleApiClient.OnConnectionFailedListener  {

    SharedPreferences pref_login;
    SharedPreferences.Editor editor_login;
    GoogleApiClient googleApiClient;
    CompactCalendarView compactCalendarView;
    LinearLayout lin_welcome;
    String Email;
    String Order_ID="";
AVLoadingIndicatorView indicatorView;
    FloatingActionButton fab_add,fab_edit,fab_cancel,fab_open;
    LinearLayout lin_add,lin_edt,lin_cancel;
    TextView txt_dialog_meal,txt_dialog_lunch,txt_dialog_dinner,txt_dialog_no_of_tiffin,txt_dialog_start_date,txt_dialog_end_date;

ArrayList<Long> list_cancel_dates;
AVLoadingIndicatorView loadingIndicatorView;
    HashMap<String,ArrayList<Long>> List_Cancel_dates;
    ArrayList<String> list_order_Id=new ArrayList<String>();
    ArrayList<String> list_tiffin_type=new ArrayList<String>();
    ArrayList<String> list_Lunch=new ArrayList<String>();
    ArrayList<String> list_dinner=new ArrayList<String>();
    ArrayList<String> list_day=new ArrayList<String>();
    ArrayList<String> list_week=new ArrayList<String>();
    ArrayList<String> list_month=new ArrayList<String>();
    ArrayList<String> list_no_tiffin=new ArrayList<String>();
    ArrayList<String> list_start_date=new ArrayList<>();
    ArrayList<String> list_end_date=new ArrayList<>();

    public static final String URL_Get_ORDER="http://tiffinwala.16mb.com/fetch_Order.php";
    public static final String URL_Get_Cancel_Order="http://tiffinwala.16mb.com/fetch_cancel_date.php";
    JSONParser jParser =new JSONParser();
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

       // this.setTitle("Tiffin Calander View");


        loadingIndicatorView=(AVLoadingIndicatorView) findViewById(R.id.avi_welcome);
        lin_welcome=(LinearLayout)findViewById(R.id.activity_welcome_lin);

        loadingIndicatorView.show();

        compactCalendarView=(CompactCalendarView)findViewById(R.id.compactcalendar_view);
        compactCalendarView.setFirstDayOfWeek(Calendar.MONDAY);
        compactCalendarView.setUseThreeLetterAbbreviation(true);
        final String[] SelectedMonth = new String[]{"January", "February", "March", "April",
                "May", "June", "July", "Augest", "September", "October", "November", "December"};
        Calendar calendar=Calendar.getInstance();
        int Month=calendar.get(Calendar.MONTH);
        int Year=calendar.get(Calendar.YEAR);
        this.setTitle(SelectedMonth[Month]);

       // indicatorView=(AVLoadingIndicatorView)findViewById(R.id.activity_welcome_loader);
       final Animation show_button= AnimationUtils.loadAnimation(Welcome.this,R.anim.show_floating_button);
        final Animation Hide_button= AnimationUtils.loadAnimation(Welcome.this,R.anim.hide_floating_button);
        final Animation show_layout= AnimationUtils.loadAnimation(Welcome.this,R.anim.show_layout);
        final Animation Hide_layout= AnimationUtils.loadAnimation(Welcome.this,R.anim.hide_layout);

        pref_login=getSharedPreferences("Login",MODE_PRIVATE);
        editor_login=pref_login.edit();

        fab_add=(FloatingActionButton)findViewById(R.id.activity_welcome_fab_add_order);
        fab_edit=(FloatingActionButton)findViewById(R.id.activity_welcome_fab_edit_order);
        fab_cancel=(FloatingActionButton)findViewById(R.id.activity_welcome_fab_cancel_order);
        fab_open=(FloatingActionButton)findViewById(R.id.activity_welcome_fab_open);

        lin_add=(LinearLayout)findViewById(R.id.activity_welcome_lin_add_order);
        lin_edt=(LinearLayout)findViewById(R.id.activity_welcome_lin_edit_order);
        lin_cancel=(LinearLayout)findViewById(R.id.activity_welcome_lin_cancel_order);

        fab_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Welcome.this,Edit_Tiffin.class));
            }
        });
        fab_open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(lin_add.getVisibility()==View.VISIBLE){

                    fab_open.startAnimation(Hide_button);
                    lin_edt.startAnimation(Hide_layout);
                    lin_cancel.startAnimation(Hide_layout);
                    lin_add.startAnimation(Hide_layout);

                    lin_add.setVisibility(View.INVISIBLE);
                    lin_edt.setVisibility(View.INVISIBLE);
                    lin_cancel.setVisibility(View.INVISIBLE);


                }
                else {


                    lin_add.setVisibility(View.VISIBLE);
                    lin_edt.setVisibility(View.VISIBLE);
                    lin_cancel.setVisibility(View.VISIBLE);

                    fab_open.startAnimation(show_button);
                    lin_add.startAnimation(show_layout);
                    lin_edt.startAnimation(show_layout);
                    lin_cancel.startAnimation(show_layout);

                }
            }
        });

        fab_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Welcome.this,Cancel_Tiffin.class);
                startActivity(intent);

            }
        });

        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor_login.putBoolean("Add_Order",true);
                editor_login.commit();

                Intent intent=new Intent(Welcome.this,TiffinSelection.class);
                startActivity(intent);

            }
        });

        String LogedIn_User=pref_login.getString("LogedIn_User",null);
        String LogedIn_User_G=pref_login.getString("LogedIn_User_G",null);

        if(LogedIn_User_G!=null)
        {
           Email=LogedIn_User_G;

        }
        else {
           Email=LogedIn_User;
        }

        new FetchOrderTiffin().execute();




      /*  long date=CoverLong("30-03-2017");
        Event ev2 = new Event(Color.BLUE, date,"Hello World..!!");
        compactCalendarView.addEvent(ev2);


        long date1=CoverLong("31-03-2017");
        Event ev3 = new Event(Color.BLUE, date1,"Hello Mits..!!");
        compactCalendarView.addEvent(ev3);
*/
        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                List<Event> events = compactCalendarView.getEvents(dateClicked);
                String Tiffin_Details="Selected Day Tiffin:";
             //   Toast.makeText(Welcome.this, ""+events.size(), Toast.LENGTH_SHORT).show();

for(int i=0;i<events.size();i++){

    dialog=new Dialog(Welcome.this);
    dialog.setContentView(R.layout.date_order_view);


    txt_dialog_meal=(TextView)dialog.findViewById(R.id.date__order_view_txt_mealtype);
    txt_dialog_lunch=(TextView)dialog.findViewById(R.id.date__order_view_txt_lunch);
    txt_dialog_dinner=(TextView)dialog.findViewById(R.id.date__order_view_txt_dinner);
    txt_dialog_no_of_tiffin=(TextView)dialog.findViewById(R.id.date__order_view_txt_no_of_tiffin);
    txt_dialog_start_date=(TextView)dialog.findViewById(R.id.date__order_view_txt_start_date);
    txt_dialog_end_date=(TextView)dialog.findViewById(R.id.date__order_view_txt_end_date);





   // Toast.makeText(Welcome.this, ""+events.get(i).toString(), Toast.LENGTH_SHORT).show();

    String details=events.get(i).getData().toString();
    String[] details_arr=details.split(" ");


    if(events.get(i).getColor()==Color.RED)
    {
        txt_dialog_meal.setText(details_arr[0]+" "+details_arr[1]+" \n(Cancelled)");
    }
    else {
        txt_dialog_meal.setText(details_arr[0]+" "+details_arr[1]);
    }



    if(details_arr[2].equals("0")){
        txt_dialog_lunch.setVisibility(View.GONE);
    }
    else {
        txt_dialog_lunch.setText("Lunch");
    }

    if(details_arr[3].equals("0")){
        txt_dialog_dinner.setVisibility(View.GONE);
    }
    else {
        txt_dialog_dinner.setText("Dinner");
    }

    txt_dialog_no_of_tiffin.setText("Tiffin : "+details_arr[4]);


    String st_date=convertDate(details_arr[5]);


    txt_dialog_start_date.setText("Start : "+st_date);

    String e_date=convertDate(details_arr[6]);

    txt_dialog_end_date.setText("Till : "+e_date);

    dialog.setTitle("Details");
    dialog.show();


    Handler handler = new Handler();
    handler.postDelayed(new Runnable() {
        public void run() {
            dialog.dismiss();
        }
    }, 10000);

  //  Toast.makeText(Welcome.this, "heyy", Toast.LENGTH_SHORT).show();


}


            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {

                setTitle(SelectedMonth[firstDayOfNewMonth.getMonth()]);
            }
        });


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();


        googleApiClient=new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


    }

    @Override
    public void onBackPressed() {

        startActivity(new Intent(Welcome.this,Welcome.class));
        super.onBackPressed();
    }

   /* private long CoverLong(Date s) {

        long milliseconds=0;


        try {
            Date d = f.parse(s);
            milliseconds = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return milliseconds;
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_select__tiifin, menu);

        return super.onCreateOptionsMenu(menu);
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case R.id.action_menu_logout:
            {
                String LogedIn_User=pref_login.getString("LogedIn_User",null);
                String LogedIn_User_G=pref_login.getString("LogedIn_User_G",null);

                if(LogedIn_User_G!=null)
                {
                    signOut();
                    editor_login.clear();
                    editor_login.commit();

                }
                else {
                    editor_login.clear();
                    editor_login.commit();
                    startActivity(new Intent(Welcome.this,LoginActivity.class));
                }
                return true;
            }

            case R.id.action_menu_profile:{

                startActivity(new Intent(Welcome.this,Profile.class));
            }

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {

                        startActivity(new Intent(Welcome.this,LoginActivity.class));
                    }
                });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, ""+connectionResult, Toast.LENGTH_SHORT).show();
    }

    class FetchOrderTiffin extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {

       //   indicatorView.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        /**
         * getting All products from url
         */
        @Override
        public String doInBackground(String... args) {
            // creating new product in background thread


            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();


                params.add(new BasicNameValuePair("Email",Email));


                JSONObject json = jParser.makeHttpRequest(URL_Get_ORDER, "POST", params);


                int success1 = Integer.parseInt(json.getString("success"));
                Log.e("data", params.toString());
                Log.d("success", json.toString());
                if (success1 == 1) {

                    JSONArray jsonArray=json.getJSONArray("Order");
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject=jsonArray.getJSONObject(i);



                        list_order_Id.add(jsonObject.getString("Order_ID"));
                        list_tiffin_type.add(jsonObject.getString("Tiffin_Type"));
                        list_Lunch.add(jsonObject.getString("Lunch"));
                        list_start_date.add(jsonObject.getString("Start_Date"));
                        list_dinner.add(jsonObject.getString("Dinner"));
                        list_day.add(jsonObject.getString("Day"));
                        list_week.add(jsonObject.getString("Week"));
                        list_month.add(jsonObject.getString("Month"));
                        list_no_tiffin.add(jsonObject.getString("Tiffin_Count"));
                        list_end_date.add(jsonObject.getString("End_Date"));



                    }

                }


                else {

                    String success2 = json.getString("success");
                    Log.d("success", success2);

                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String result) {


//SetCancelDate();
            new FetchCancelDetails().execute();

            super.onPostExecute(result);

        }
    }



    private void SetCancelDate() {

        for(int i=0;i<list_tiffin_type.size();i++){
            Order_ID=list_order_Id.get(i).toString();

String str="";
            str+=list_tiffin_type.get(i);

          str+=" "+list_Lunch.get(i)+" "+list_dinner.get(i);

            str+=" "+ list_no_tiffin.get(i)+" "+list_start_date.get(i)+" "+list_end_date.get(i);

            //   new FetchCancelDetails().execute();
            int NO_DAY=0;
            Calendar start = Calendar.getInstance();
            String Date=list_start_date.get(i);
            SimpleDateFormat formatter1=new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH);
            try {
                Date date=formatter1.parse(Date);
                start.setTime(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if(!list_day.get(i).equals("0"))
            {
                NO_DAY=Integer.parseInt(list_day.get(i));
            }
            else if(!list_week.get(i).equals("0"))
            {
                NO_DAY=Integer.parseInt(list_week.get(i))*7;
            }
            else if(!list_month.get(i).equals("0"))
            {
                NO_DAY=Integer.parseInt(list_month.get(i))*30;
            }


            for(int j=0;j<NO_DAY;j++){

                Date targetDay = start.getTime();
                // Do Work Here

                start.add(Calendar.DATE, 1);

                ArrayList<Long> dates=List_Cancel_dates.get(Order_ID);



                if(!dates.contains(targetDay.getTime()))
                {

                    Event ev2 = new Event(Color.CYAN,targetDay.getTime(),str);
                    compactCalendarView.addEvent(ev2);
                }
                else {
                    Event ev2 = new Event(Color.RED,targetDay.getTime(),str);
                    compactCalendarView.addEvent(ev2);
                }


            }
        }

        loadingIndicatorView.hide();
        loadingIndicatorView.setVisibility(View.INVISIBLE);
        lin_welcome.setVisibility(View.VISIBLE);



    }

    public String convertDate(String data) {


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date testDate = null;
        try {
            testDate = sdf.parse(data);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        String newFormat = formatter.format(testDate);
       return newFormat;

    }

    class FetchCancelDetails extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {


            super.onPreExecute();
        }

        /**
         * getting All products from url
         */
        @Override
        public String doInBackground(String... args) {
            // creating new product in background thread
            List_Cancel_dates=new HashMap<String, ArrayList<Long>>();
            for (int j = 0; j < list_tiffin_type.size(); j++)
            {

                ArrayList<Long>  list_dates=new ArrayList<Long>();
                try {

                    List<NameValuePair> params = new ArrayList<NameValuePair>();

                    String OID=list_order_Id.get(j).toString();

                    params.add(new BasicNameValuePair("Order_ID", OID));

                    JSONParser jParser1 = new JSONParser();

                    JSONObject json = jParser1.makeHttpRequest(URL_Get_Cancel_Order, "POST", params);



                    int success1 = Integer.parseInt(json.getString("success"));
                    Log.e("data", params.toString());
                    Log.d("success", json.toString());

                    if (success1 == 1) {

                        list_cancel_dates = new ArrayList<Long>();
                        JSONArray jsonArray = json.getJSONArray("Cancel_Date");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            Date date = converStringToDate(jsonObject.getString("Date"));

                           list_dates.add(date.getTime());

                        }
                    } else {

                        String success2 = json.getString("success");
                        Log.d("success", success2);

                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

           List_Cancel_dates.put(list_order_Id.get(j),list_dates);

        }
            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String result) {



            super.onPostExecute(result);
            SetCancelDate();

     //      indicatorView.setVisibility(View.GONE);
        }
    }

    private Date converStringToDate(String cancel_date) {

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(cancel_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}