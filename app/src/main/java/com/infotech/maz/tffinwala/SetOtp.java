package com.infotech.maz.tffinwala;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SetOtp extends AppCompatActivity {

    EditText edt_otp;
    Button btn_submit;
    String email="";
    String OTP="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_otp);

        OTP=getIntent().getStringExtra("OTP");

        edt_otp=(EditText)findViewById(R.id.activity_set_otp_edt_OTP);
        btn_submit=(Button)findViewById(R.id.activity_set_otp_btn_submit);

btn_submit.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        if(OTP.equals(edt_otp.getText().toString())){
            email=getIntent().getStringExtra("Email");
            Intent intent=new Intent(new Intent(SetOtp.this,Change_Password.class));
            intent.putExtra("Email",email);
            startActivity(intent);
        }
        else {
            Toast.makeText(SetOtp.this, "OTP not Verified..!!!", Toast.LENGTH_SHORT).show();
        }
    }
});



    }
}
